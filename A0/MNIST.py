import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


# Load the data from the MNIST handwritten digit data set
mnist = tf.keras.datasets.mnist

# x contains the data (28x28 grayscale images of handwritten digits), y contains the labels (the digit each image represents)
(x_train, y_train), (x_test, y_test) = mnist.load_data()
print ("training images shape: ", x_train.shape)
print ("training labels shape: ", y_train.shape)
print ("test images shape: ", x_test.shape)
print ("test labels shape: ", y_test.shape)

# Normalize images from [0,255) to [0,1)
x_train, x_test = x_train / 255.0, x_test / 255.0

# Build the network using the keras API
model = tf.keras.models.Sequential([                # A sequential model just has a sequence of layers; there are no connections between non-sequential layers
  tf.keras.layers.Flatten(input_shape=(28, 28)),    # The input is a 2D image; this makes sure it can connect to a 1D sequence of nodes
  tf.keras.layers.Dense(128, activation='relu'),    # A layer of 128 fully connected nodes with linear rectifier activation functions (f(x<0)=0, f(x>0)=x)
  tf.keras.layers.Dropout(0.2),                     # Dropout between the previous and following layers; every node in the previous layer has a 20% chance
                                                    # of passing on 0 during training. This helps against overfitting as no node is allowed to be crucial
  tf.keras.layers.Dense(10, activation='softmax')   # Dense connection to the output nodes; each node represents a possible label, and the highest output
])                                                  # output is the classification. Softmax is yet another activation function, often used as output of
                                                    # classification networks.

model.compile(optimizer='adam',                         # This compiles the model, ensuring it can run. Adam is an optimization algorithm, and improvement
              loss='sparse_categorical_crossentropy',   # on gradient descent. The loss used is appropriate when the labels are integers.
              metrics=['accuracy'])                     # This model optimizes accuracy.

# Fit the model to the training data; go through the data 5 times, also known as 5 epochs of training.
model.fit(x_train, y_train, epochs=5)

# Evaluate the model on the test data
result = model.evaluate(x_test, y_test, verbose=0)

print ("The loss on the test data was: ", result[0])
print ("The accuracy on the test data was: ", result[1])

# Predict the outputs for the test data. This outputs 10 values for each images, corresponding to the 'likelihood' of that image being a certain digit.
predictions = model.predict(x_test, verbose=0)

predicted_digits = np.argmax(predictions, axis=1)


# Construct confusion matrix
confusion_matrix = np.zeros((10, 10))

for i in range(10):

    for j in range(10):

        # confusion_matrix[i,j] contains the number of digits predicted to be i but that have labels j
        mask = predicted_digits == i

        confusion_matrix[i,j] = np.sum(y_test[ mask ] == j)


# Plot confusion matrix
fig = plt.figure(1)
ax = fig.add_subplot(111)

cax = ax.imshow(confusion_matrix, interpolation='nearest', cmap=plt.cm.Blues)
fig.colorbar(cax, label='counts')

ax.set_xlim(-0.5, 9.5)
ax.set_ylim(-0.5, 9.5)

ax.set_xticks(np.arange(10), str(np.arange(10)))
ax.set_yticks(np.arange(10), str(np.arange(10)))

for i in range(10):
	for j in range(10):
		if confusion_matrix[i,j] > np.max(confusion_matrix)/2.:
			c="white"
		else:
			c="black"

		ax.text(j, i, str(confusion_matrix[i,j]), horizontalalignment='center', color=c)

ax.set_ylabel('Predicted Digit')
ax.set_xlabel('True Digit')

plt.tight_layout()

plt.show()
