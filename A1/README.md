# Forward Integrator

The `traj_calc.py` file integrates SSSBs of a number designation, specified in the 6th column of the `obs_matrix.npy` and `pho_matrix.npy` matrices, forward in time. It uses the *AMUSE* framework which can now be easily installed with `pip install amuse` command on most machines. For more information about *AMUSE* click here [here](https://amusecode.github.io/). On observatory pc's, *AMUSE* is already installed, and can be run using `mpiexec amuse script.py` (instead of the normal `python script.py`).

The input to the program should be a plain text file where every row is an integer number designation of an asteroid. Additionally, you need to specify the output file name and path along with the location of the `data` folder which contains the cartesian coordinates necessary to perform the simulations. The total integration time and time step can also be specified although they are already set to the values requested for the assignment. 

The output file should contain 3 columns. The first column specifies the asteriods number designation, the second column the year of the closest approach and the third column specifies the distance of the closest approach in AU.

# Number to name converter

Although it is not necessary to find the permanent name designation of the SSSBs for this assignment you can do so with the `number_name_converter.py` script, which simply creates a dictionary where the keys are the numbers and the values are the permanent name designation. To use the dictionary, simply input the number of the asteriod *as a string* and the output will be its name. You can then look up more information about this asteroid on the Horizons database. 
