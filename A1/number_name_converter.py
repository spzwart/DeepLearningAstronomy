"""
-Creates a dictionary to convert the asteriods number designations to their names
-The numbers must be put as strings!

*More information in the README.md

"""

__author__      = "John D. Hefele"
__email__       = "jdavidhefele@gmail.com"

def create_dic_nameNumber(pn_number_names):
    numberNames=open(pn_number_names,'r')
    names_number={}
    for i,row in enumerate(numberNames):
        if i!=0:
            strip=row.strip('\n').split('\t')
            number=strip[0]
            #print(type(number))
            name=strip[1]
            if name!='NF':
                name=name[:-1] #Gets rid of space
            names_number[number]=name
    return names_number

if __name__=='__main__':

    pn_numberNames='data/number_names.txt'
    names_number=create_dic_nameNumber(pn_numberNames)
    number=str(351321)
    name=names_number[number]
    print(name)








