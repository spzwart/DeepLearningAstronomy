Week 1 (16 March): Intro to NN
     	    	   Split-off in teams
Week 2 (23 March): Project topics in NN start projects
                   Cloud computing (John Hefele)
		   Advanced Recurent Networks (Wojtek)

March 30: no lecture		   

Week 3 (6 April):  Convolutional (Wojtek), Near-Earth Asteroids classification (John)
Week 4 (13 April): Holger Hoos: model tuning hyperparameters
week 5 (20 April): (Jeroen Bedorf)  and Avertarial search (John)

April 27: no lecture

week 6 (4 May): ...
week 7 (11 May): present projects (two team)

Bonus grade when final paper is being written.

