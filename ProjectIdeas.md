**LSTM to Predict Chaotic Systems**- Max can elaborate

**Neural Network for radio-ionosphere calibrations**-Josh can elaborate
Project 1:
Much like optical telescopes have to deal with turbulent layers between 
themselves and the celstial radiation, radio interferometers also suffer from 
path length distortions from the ionosphere. Unlike optical astronomers, in
the radio we do image forming after collecting the data, since we save the
wave information. Imaging requires multiple steps of calibration and deconvol-
ution, and requires tweaking the complex gains of the correlation products.
The contribution of the ionosphere is to make sources appear to move around,
and fluctuate in brightness (not a good thing if you want high contrast).

The goal of this project is to produce estimates of the phase distortions in all
directions, for each antenna, frequency, and time in a real LOFAR observation.
The data and tools you'll be given are:

1. A set of noisy point-wise estimates of phase distortions in a set of ~40 
   directions (20deg^2) for each antenna, time, and frequency. Note the phase is 
   wrapped. There is no ground truth to this dataset.
2. A simulator that can simulate phase screens for each antenna and frequency,
   BUT the simulator doesn't know about time, AND the hyper-parameters of the 
   simulation are uncertain.

The goal is to produce better estimates of the phase distortions in those 40
directions. Validation is a very time consuming process, which requires applying
those phase solutions, and re-imaging. Validation is then seeing how well the
image improves over the point-wise estimates. Due to the time involved in 
validating (~2 days with 64 cores), validation can only happen a few times.
Techniques that will likely be useful: unsupervised learning, image denoising,
recurrent prediction, Bayesian representation.

Project 2:
A second project is based on training a 3D convolutional neural network to do
tomography using the same above simulator. That is, given the phase screens,
learn the distribution of the turbulent medium creating them. It is very inter-
esting from the perspective of machine learning, which originally was heavily
focused on computer vision. Essentially, this is a deprojection problem.
The beauty of this method over project 1 method is that given the distribution
of the medium you can forward predict phases in any direction.
Techniques that will be useful here: unsupervised learning, 3D convnets, 
architecture searching, fine tuning, deconvolution, attention mechanisms.

Nota bene - If a group is sucessful at project 2, I will make sure we get a 
paper out of it.

**RNN/CNN to improve the performance of adaptive optics systems**- Vikram can elaborate
Idea 1) Large optical and infrared ground-based telescopes can be limited by the effect of atmospheric turbulence. Atmospheric turbulence results in a distorted wavefront entering the telescope.
Most Adaptive optics systems work by measuring the phase of the incoming wavefront, reconstructing this wavefront, and flattening the wavefront using a deformable mirror with the opposite surface shape.
However, there is a delay between the wavefront measurement and correction, during which the wavefront evolves. So the applied correction does not perfectly flatten the wavefront.
With recurrent neural networks, a sequence of data from the wavefront sensor can be used to predict the wavefront in advance, and hence pre-emptively correct it.
This reduces or removes servo lag errors in the optical system.

Idea 2) Using reinforcement learning, a neural network can be trained to control an AO system, using focal plane image data, along with wavefront measurements.
This type of neural network would be trained by allowing it to correct thousands of phase-screens, recording WFS data at each training step.
At the end of every training episode, the focal plane image quality would be measured (e.g. Strehl ratio could be measured) and used to reward or penalize the network.
The advantage of such a control system would be that all non common path aberrations (aberrations that occur in the science optical path but not the wavefront sensor optical path)
would be corrected by this method. 

**Generative-Adversial Network for the intelligent selection of initial conditions for star clusters**- Simon can elaborate

**Generative-Adversial Network for the generation of realistic hazardous asteroids**- A dangerous asteroid can be simulated by launching an object from Earth and integrating it backwards in time. 
However, typically asteroids generated in this manner are very different from real/observed asteroids in terms of orbital elements. A generative-adverisal network could potentially remedy this problem
by creating one network that could be used to generate simulated asteroids and the other that could be simultaneously used to  distinguish between real and simulated asteroids. These two networks could
then compete to fool the other, which could ultimately create 1 network that is very good at generating realistic examples, and another  network that is very good at  distinguishing between real and simulated objects. 

**NNExtractor**   A source extraction program that uses a Convolutional Neural Network to identify sources in data. 
We can start by just seeing if it can distinguish sources from noise and then push this idea as far as we want by seeing if it can classify different classes of sources (galaxies, stars, quasars etc) or mixtures of multiple objects in the same frame. 
It's (really..) similar to what https://arxiv.org/pdf/astro-ph/0006115.pdf has done but this paper uses three different (perceptron) neural networks that use features extracted seperately from the data. I propose we try to use a Convolutional Neural Network and only use the morphology of the sources, to have a more general architecture.

