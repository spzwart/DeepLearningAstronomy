Deep Learning in Astronomy

Teachers: Simon Portegies Zwart <spz@strw.leidenuniv.nl>
	  Wojtek Kowalczyk <w.j.kowalczyk@liacs.leidenuniv.nl>
TA: Martijn Wilhelm <wilhelm@strw.leidenuniv.nl>

Guest lecturers:
Sascha Caron: < s.caron@hef.ru.nl> https://www.nikhef.nl/~scaron/
Ln(a) Selentin <sellentin@strw.leidenuniv.nl>
Maxwell Cai
Jeroen Bedorf

John Hefele

Dates:
1. 27 September: How to do NL research project
      		 (Wojtek: single-node perceptron, convolutional NNs:
		 	  How does a machine learning project work.)
      		 Install Tensorflow etc.
		 Create computer account
		 Zeroth assignment (MNIST)
		 First assignment (HOI)
    4 October:   No lecture
   11 October:	 Martijn introduce project possibilities
2. 18 October:   Students chose topics & team selection
      		 Introduce projects 
      		 Introduction to ML (John Hefele: back-propagation,
       	      	    	  	     universal generalization theorem)
	         Present teams and their topic [question, algorithm, data]:
		 Start project
3. 25 October:	 Ln(a) Selentin [Bayasian Neural Networks]
                 Neural scientist...
4.  1 November:	 Maxwell Cai [Reinforced learning Newton]
5.  8 November:	 Lecture Wojtek
6. 15 November:	 Jeroen Bedorf
7. 22 November:	 ...
8.  6 December:	 Project Presentations


List of possible problems:
List of classes of algorithms:
List of data resources:

Example projects:

Potential side topics:
Evolutionary Hardware (c. Elegans)
Neural Systems [Asked Han de Winde (Biology)]
Genetic programming [Thomas Beack]
Genetic Algorithms [...]
Evolutionary Computing 

Course material:
Publicly available books on machine learning:
http://www.deeplearningbook.org/
http://themlbook.com/wiki/doku.php
http://www.cs.huji.ac.il/~shais/UnderstandingMachineLearning/copy.html

Judgement:
Reproducibility
Code quality
Data understanding
Neural Network knowhow

Complete problem to solve:
How far did they get.
What techniques used.
How well is the code readable (documented)
Quality of the report
Quality of the presentation

Grading:
Assignment 0:  0%
Assignment 1: 10%
Project 1:    
 Paper:        70%
 Presenation:  20% (10 or 15 min)
