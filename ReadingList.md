http://neuralnetworksanddeeplearning.com - This gives a clear-practical introduction to neural networks. I would strongly recommend chapter 1 and 3. Chapter 2 gives an interesting overview of the back-propogation algorithm, but for most it will be a little to thorough. 

http://colah.github.io/posts/2015-08-Understanding-LSTMs/ - Gives a great introduction to LSTM networks. Very clear and well-written without almost any equations. 

http://www.deeplearningbook.org/ - For all students I would recommend the introduction. The rest of the books is optional; it offers allot of mathematical rigor, which is not completely necessary to understand unless you are a researcher in the field. 

https://medium.com/@ageitgey/machine-learning-is-fun-part-3-deep-learning-and-convolutional-neural-networks-f40359318721 - A practical introduction to CNNs without allot of nice pictures to help illustrates points. 

http://numericinsight.com/uploads/A_Gentle_Introduction_to_Backpropagation.pdf - This is what really helped me wrap my head around the backpropogation algorithm.

http://www.cs.toronto.edu/~hinton/absps/momentum.pdf - Dense academic paper, but it describes the important concept of momentum in deep learning.

http://ufldl.stanford.edu/tutorial/unsupervised/Autoencoders/ - A practical introduction to auto-encoders

https://www.kaggle.com/ - A real interesting site that host data-science competitions. Some of them are for some serious cash (million dollars +). 

https://arxiv.org/pdf/1406.2661.pdf - A dense academic paper, but it explains a new up and coming way of training NN by having one network guess frauds and another neural network try to generate realistic examples (Generative Adversarial Net)

http://karpathy.github.io/2015/05/21/rnn-effectiveness/ - A fun read over RNNs. Gives some examples of application, one is a Shakespeare generator. 

https://www.youtube.com/watch?v=F1ka6a13S9I&index=2&list=PLTyGOFI9_3Vv11MMNVFCGxmUve5pMPSMF - I could not recommend this video and the video's linked enough. They focus on our goal, getting scientist prepared to starting using NN as quickly as possible. The talks are given by Andrew Ng who is known his great, simple, explanations of abstract NN concepts. 